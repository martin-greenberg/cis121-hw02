
import static java.lang.Math.pow;

/**
 * @author nicmeyer
 */
public final class Circle extends Figure {
	private final double radius;
	private final Point center;

	/**
	 * Given a radius and center, constructs a circle and its bounding box
	 * 
	 * @param radius
	 * @param center
	 * @throws NullPointerException
	 */
	public Circle(double radius, Point center) {
		super(computeBoundingBox(radius, center)); // see constructor in superclass {@link Figure}
		if (center == null) {
			throw new NullPointerException();
		}
		this.radius = radius;
		this.center = center;
	}

	@Override
	public String toString() {
		return "Circle of radius " + radius + " centered at " + center + ".";
	}

	/**
	 * Auxiliary method that computes a bounding box for a circle of given
	 * radius and center Used in the constructor above.
	 * 
	 * @param radius
	 * @param center
	 * @throws NullPointerException
	 */
	private static BoundingBox computeBoundingBox(double radius, Point center) {
                if (center == null) {
			throw new NullPointerException();
		}
                double left = center.x-radius;
                double right = center.x+radius;
                double bottom = center.y-radius;
                double top = center.y+radius;
		return new BoundingBox(left, right, bottom, top);
	}

	@Override
	protected boolean contains(Point point) {
		return (pow(point.x,2.0) + pow(point.y,2.0) <= pow(this.radius,2.0));
	}
}
