import java.util.Collection;
import java.util.Iterator;

public class BoundingBox { // Note that it does NOT extend Figure
	public final double left, right, top, bottom;

	/**
	 * Constructs a bounding box with the given left, right, bottom, and top
	 * coordinates. The x-axis goes from left to right. The y-axis goes from
	 * bottom to top. Coordinates may be negative.
	 * 
	 * @param left
	 * @param right
	 * @param bottom
	 * @param top
	 */
	public BoundingBox(double left, double right, double bottom, double top) {
		if (left > right || bottom > top) {
			throw new IllegalArgumentException();
		}
		this.left = left;
		this.right = right;
		this.bottom = bottom;
		this.top = top;
	}

	/**
	 * Constructs a bounding box for a single point.
	 * 
	 * @param point
	 * @throws NullPointerException
	 */
	public BoundingBox(Point point) {
		if (point == null) {
			throw new NullPointerException();
		}
		this.left = point.x;
		this.right = point.x;
		this.top = point.y;
		this.bottom = point.y;
	}

	/**
	 * Provides the corners of the bounding box in clockwise order, starting at
	 * with the bottom left.
	 * 
	 * @return an array of four {@link Point}s corresponding to the corners of
	 *         the bounding box
	 */
	public Point[] getPoints() {
		return new Point[] { new Point(left, bottom), new Point(left, top),
				new Point(right, top), new Point(right, bottom) };
	}

	/**
	 * Checks whether the given point is contained within the bounding box.
	 * Points on the boundary of the box are considered to be contained within
	 * it.
	 * 
	 * @return whether the given point is contained within the bounding box
	 * @throws NullPointerException
	 */
	public boolean contains(Point point) {
		if (point == null) {
			throw new NullPointerException();
		}
		return point.x >= left && point.x <= right && point.y >= bottom
				&& point.y <= top;
	}

	/**
	 * Combines a collection of bounding boxes into the smallest one that covers
	 * all the ones in the collection.
	 * 
	 * @param boxes
	 * @return the minimal bounding box containing the given bounding boxes, or
	 *         null if the collection is empty
	 * @throws NullPointerException
	 */
	public static BoundingBox union(Collection<BoundingBox> boxes) {
		if(boxes == null){
                    throw new NullPointerException();
                }
                if(boxes.isEmpty()){
                    return null;
                }
                Iterator<BoundingBox> boxIterator = boxes.iterator();
                BoundingBox currBox = boxIterator.next();
                
                double left = currBox.left;
                double right = currBox.right;
                double bottom = currBox.bottom;
                double top = currBox.top;
                
                while(boxIterator.hasNext()){
                    currBox = boxIterator.next();
                    left = (currBox.left > left) ? left : currBox.left;
                    right = (currBox.right < right) ? right : currBox.right;
                    bottom = (currBox.bottom > bottom) ? bottom : currBox.bottom;
                    top = (currBox.top < top) ? top : currBox.top;
                }
                
                return new BoundingBox(left, right, bottom, top);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(bottom);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(left);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(right);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(top);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BoundingBox other = (BoundingBox) obj;
		if (Double.doubleToLongBits(bottom) != Double
				.doubleToLongBits(other.bottom)) {
			return false;
		}
		if (Double.doubleToLongBits(left) != Double
				.doubleToLongBits(other.left)) {
			return false;
		}
		if (Double.doubleToLongBits(right) != Double
				.doubleToLongBits(other.right)) {
			return false;
		}
		if (Double.doubleToLongBits(top) != Double.doubleToLongBits(other.top)) {
			return false;
		}
		return true;
	}

}