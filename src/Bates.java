public class Bates {
	/**
	 * Returns the largest rectangular region in the display consisting of only
	 * 1s. Takes the display as a two dimensional array of pixel values that are
	 * either 0s or 1s in column-major format. Consequently display[4][2] is the
	 * pixel value at the point (4,2).
	 * 
	 * @return the maximal rectangular region consisting of only 1s, or null if
	 *         none exists
	 * @throws NullPointerException
	 */
	public static BatesRectangle findMaximal(int[][] display) {
		// Unimplemented
		return null;
	}
}
