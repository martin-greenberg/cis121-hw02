/**
 * @author nicmeyer
 * 
 */
public interface Intersectable<T extends Figure> {
	/**
	 * @return whether this {@link ImmutableFigure} intersects with another one,
	 *         i.e. whether it contains any of the same points
	 * @throws NullPointerException
	 */
	public boolean intersects(T t);
}
