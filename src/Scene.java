import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * A class representing a collection of {@link Figure}s. The bounding box of a
 * scene is defined as the minimal rectangle containing all of the bounding
 * boxes for each Figure in the Scene.
 * 
 * @author nicmeyer
 */
public class Scene extends Figure {
	private final Set<Figure> shapes;

	/**
	 * Constructs a Scene from a set of {@link Figure}s.
	 * 
	 * @throws NullPointerException
	 * @throws IllegalArgumentException
	 */
	public Scene(Set<Figure> shapes) {
		super(computeBoundingBox(shapes));// see constructor in superclass {@link Figure}
		if (shapes == null) {
			throw new NullPointerException();
		}
		if (shapes.isEmpty()) {
			throw new IllegalArgumentException();
		}
		this.shapes = shapes;
	}

	/**
	 * Auxiliary method that computes a bounding box for a Scene given by a set
	 * of {@link Figure}s. Used in the constructor above.
	 * 
	 * @param Set
	 *            <Figure> shapes
	 * @throws NullPointerException
	 * @throws IllegalArgumentException
	 */
	private static BoundingBox computeBoundingBox(Set<Figure> shapes) {
		if (shapes == null) {
			throw new NullPointerException();
		}
		if (shapes.isEmpty()) {
			throw new IllegalArgumentException();
		}
                Iterator<Figure> shapesIterator = shapes.iterator();
                HashSet<BoundingBox> boxes = new HashSet<BoundingBox>();
                Figure currFigure = shapesIterator.next();
                do {
                    boxes.add(shapesIterator.next().boundingBox);
                } while(shapesIterator.hasNext());
                        
		return BoundingBox.union(boxes);   
	}

	@Override
	protected boolean contains(Point point) {
                if(point == null){
                    throw new NullPointerException();
                }
		Iterator<Figure> shapesIterator = shapes.iterator();
                do {
                    if(shapesIterator.next().contains(point)){
                        return true;
                    }
                } while (shapesIterator.hasNext());
                
		return false;
	}
}
