// Do not modify this class

public class BatesRectangle {
	public final int upperLeftX, upperLeftY, lowerRightX, lowerRightY;

	public BatesRectangle(int upperLeftX, int upperLeftY, int lowerRightX,
			int lowerRightY) {
		this.upperLeftX = upperLeftX;
		this.upperLeftY = upperLeftY;
		this.lowerRightX = lowerRightX;
		this.lowerRightY = lowerRightY;
	}
}
