public abstract class Figure {
	public final BoundingBox boundingBox;

	/**
	 * Creates an {@link Figure}. An {@link Figure} corresponds to a set of
	 * points in the Euclidean plane.
	 * 
	 * These sets are closed (they contain their boundary points).
	 * 
	 * @throws NullPointerException
	 */
	public Figure(BoundingBox boundingBox) {
		if (boundingBox == null) {
			throw new NullPointerException();
		}
		this.boundingBox = boundingBox;
	}

	/**
	 * Checks whether the given point is contained within the figure in a manner
	 * specific to each kind of figure.
	 * 
	 * @return whether the given point is contained within the figure
	 * @throws NullPointerException
	 */
	protected abstract boolean contains(Point point);

	/**
	 * Checks whether the given point is contained within the figure by using
	 * the bounding box when possible. When not possible, calls
	 * {@link #contains(Point)}.
	 * 
	 * @return whether the given point is contained within the figure
	 * @throws NullPointerException
	 */
	public boolean containsUsingBox(Point point) {
            if(point == null){
                throw new NullPointerException();
            }
            if(this.boundingBox.contains(point)){
                return this.contains(point);
            } else {
                return false;
            }
	}
}
