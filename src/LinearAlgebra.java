public class LinearAlgebra {

	/**
	 * Accepts two vectors of the same length, and returns their sum.
	 * 
	 * @throws IllegalArgumentException
	 * @throws NullPointerException
	 */
    
	public static double[] add(double[] v1, double[] v2) {
		if (v1 == null || v2 == null) {
			throw new NullPointerException();
		}
		if (v1.length != v2.length) {
			throw new IllegalArgumentException();
		}
		double[] result = new double[v1.length];
		for (int i = 0; i < v1.length; i++) {
			result[i] = v1[i] + v2[i];
		}
		return result;
	}

	/**
	 * Accepts two vectors of the same length, and returns their difference.
	 * 
	 * @throws IllegalArgumentException
	 * @throws NullPointerException
	 */
	public static double[] subtract(double[] v1, double[] v2) {
		if (v1 == null || v2 == null) {
			throw new NullPointerException();
		}
		if (v1.length != v2.length) {
			throw new IllegalArgumentException();
		}
		double[] result = new double[v1.length];
		for (int i = 0; i < v1.length; i++) {
			result[i] = v1[i] - v2[i];
		}
		return result;
	}

	/**
	 * Accepts two vectors of the same length, and returns their dot product.
	 * 
	 * @throws IllegalArgumentException
	 * @throws NullPointerException
	 */
	public static double dotProduct(double[] v1, double[] v2) {
		if (v1 == null || v2 == null) {
			throw new NullPointerException();
		}
		if (v1.length != v2.length) {
			throw new IllegalArgumentException();
		}
		double result = 0;
		for (int i = 0; i < v1.length; i++) {
			result += v1[i] * v2[i];
		}
		return result;
	}

	/**
	 * Accepts two vectors of length 3, and returns their cross product.
	 * 
	 * @throws IllegalArgumentException
	 */
	public static double[] crossProduct(double[] v1, double[] v2) {
		if (v1 == null || v2 == null) {
			throw new NullPointerException();
		}
		if (v1.length != 3 || v2.length != 3) {
			throw new IllegalArgumentException();
		}
		return new double[] { v1[1] * v2[2] - v1[2] * v2[1],
				v1[2] * v2[0] - v1[0] * v2[2], v1[0] * v2[1] - v1[1] * v2[0] };
	}
}