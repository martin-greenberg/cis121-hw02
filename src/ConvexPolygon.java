import static java.lang.Math.pow;

/**
 * @author nicmeyer
 * 
 */
public final class ConvexPolygon extends Figure implements
		Intersectable<ConvexPolygon> {
	final static float RAD2DEG = 180.0f / 3.14159f;
	private final Point[] vertices;

	/**
	 * Constructs a ConvexPolygon from an array of its vertices in clockwise
	 * order. The convex polygon is closed, i.e. it contains its boundary
	 * points.
	 * 
	 * @param Point
	 *            [] vertices Vertices, in clockwise order
	 * @throws IllegalArgumentException
	 *             Thrown if the polygon has less than three vertices or if the
	 *             given vertices do not form a convex polygon
	 * @throws NullPointerException
	 */
	public ConvexPolygon(Point[] vertices) {
		super(computeBoundingBox(vertices));
		if (vertices == null) {
			throw new NullPointerException();
		}
		if (vertices.length <= 2 || !isConvex(vertices)) {
			throw new IllegalArgumentException();
		}
		this.vertices = vertices;
	}

	/**
	 * Auxiliary method that determines whether a given array of points assumed
	 * to be the vertices of a polygon in clockwise order do determine a convex
	 * polygon.
	 * 
	 * This method uses the properties of the cross product. The cross product
	 * of two vectors is perpendicular to the plane formed by the two vectors,
	 * with direction given by the right hand rule.
	 * 
	 * Because the vertices are in clockwise order, the cross product of each
	 * pair of vectors formed by every three consecutive points in the array
	 * should be in the same direction. The cross product is allowed to be zero,
	 * to permit the case where three consecutive points are colinear.
	 */
	private boolean isConvex(Point[] vertices) {
		for (int i = 0; i < vertices.length; i++) {
			Point p1 = vertices[i];
			Point p2 = vertices[(i + 1) % vertices.length];
			Point p3 = vertices[(i + 2) % vertices.length];
			double dir = LinearAlgebra.crossProduct(new double[] { p2.x - p1.x,
					p2.y - p1.y, 0.0 }, new double[] { p3.x - p2.x,
					p3.y - p2.y, 0.0 })[2];
			if (dir > 0) {
				return false;
			}
		}
		return true;
	}

	@Override
	protected boolean contains(Point point) {
		if(point == null){
                    throw new NullPointerException();
                }
                for(int i = 0; i < this.vertices.length; i++){
                    Point p1 = vertices[i];
                    Point p2 = vertices[(i+1) % vertices.length];
                    double dir = LinearAlgebra.crossProduct(new double[] {point.x - p1.x,
                        point.y - p1.y, 0.0 }, new double[] { p2.x - point.x, 
                        p2.y - point.y, 0.0})[2];
                    if (dir < 0) {
                        return false;
                    }
                }
		return true;
	}

	/**
	 * Auxiliary method that computes a bounding box for a ConvexPolygon given
	 * by an array of points Used in the constructor above.
	 * 
	 * @param Point
	 *            [] vertices
	 * @throws NullPointerException
	 * @throws IllegalArgumentException
	 */
	private static BoundingBox computeBoundingBox(Point[] vertices) {
                if(vertices == null){
                    throw new NullPointerException();
                }
                if(vertices.length <= 2){
                    throw new IllegalArgumentException();
                }
                double left = 0.0;
                double right = 0.0;
                double bottom = 0.0;
                double top = 0.0;
		for(int i = 0; i < vertices.length; i++){
                    if(vertices[i] == null){
                        throw new NullPointerException();
                    }
                    left = (vertices[i].x >= left ? left : vertices[i].x);
                    right = (vertices[i].x <= right ? right : vertices[i].x);
                    bottom = (vertices[i].y >= bottom ? bottom : vertices[i].y);
                    top = (vertices[i].y <= top ? top : vertices[i].y);
                }
		return new BoundingBox(left, right, bottom, top);
	}

	/**
	 * Checks whether the intersection of two convex polygons is no-empty. When
	 * possible, it uses bounding boxes to determine that the intersection is
	 * empty. When the bounding boxes intersect, it uses the method described in
	 * the handout.
	 * 
	 * @param ConvexPolygon
	 *            t
	 * @return whether "this" polygon intersects t
	 * @throws NullPointerException
	 */
	public boolean intersects(ConvexPolygon t) {
                if(t == null){
                    throw new NullPointerException();
                }
                double left1 = this.boundingBox.left;
                double left2 = t.boundingBox.left;
                double right1 = this.boundingBox.right;
                double right2 = t.boundingBox.left;
                double bottom1 = this.boundingBox.bottom;
                double bottom2 = t.boundingBox.bottom;
                double top1 = this.boundingBox.top;
                double top2 = t.boundingBox.top;
                
                if(left1 > right2 || left2 > right1 || top1 > bottom2 || top2 > bottom1) return false;
		
                //if bounding boxes intersect, use separating axis theorem to check intersection
		double projA1, projA2, projB1, projB2, currProj;
                double[] v;
                
                for(int i = 0; i < this.vertices.length; i++){
                    Point p1 = this.vertices[i];
                    Point p2 = this.vertices[(i+1) % this.vertices.length];
                    v = edgeToPerpendicularVector(p1, p2);
                    //initial projection values
                    projA1 = projection(this.vertices[0], v);
                    projA2 = projection(this.vertices[0], v);
                    projB1 = projection(t.vertices[0], v);
                    projB2 = projection(t.vertices[0], v);
                    
                    //update projection values along the current edge
                    for(int j = 0; j < this.vertices.length; j++){
                        currProj = projection(this.vertices[j], v);
                        projA1 = (currProj >= projA1) ? projA1 : currProj;
                        projA2 = (currProj <= projA2) ? projA2 : currProj;
                    }
                    
                    for(int k = 0; k < t.vertices.length; k++){
                        currProj = projection(t.vertices[k], v);
                        projB1 = (currProj >= projB1) ? projB1 : currProj;
                        projB2 = (currProj <= projB2) ? projB2 : currProj;
                    }
                    
                    if(projA1 > projB2 || projB1 > projA2){
                        return false;
                    }
                }
                return true;
            }
        /**
	 * Accepts a point and a vector, and returns the projection of the point
	 * along the vector.
         * 
         * @throws NullPointerException
	 */
        
        public static double projection(Point p, double[] v1){
            if(p == null || v1 == null){
                throw new NullPointerException();
            }
            double magnitude = pow(pow(v1[0], 2.0) + pow(v1[1], 2.0), 0.5);
            //set v1 equal to its unit vector
            v1[0] = v1[0]/magnitude;
            v1[1] = v1[1]/magnitude;
            double[] v2 = {p.x, p.y};
            return LinearAlgebra.dotProduct(v1, v2);
        }
        
        /**
	 * Accepts two points, and returns the vector perpendicular
         * to the vector between them.
         * 
         * @throws NullPointerException
	 */
        public static double[] edgeToPerpendicularVector(Point p1, Point p2){
            if(p1 == null || p2 == null){
                throw new NullPointerException();
            }
            double distance = pow(pow((p2.x-p1.x), 2.0) + pow((p2.y-p1.y), 2.0), 0.5);
            double[] v = {(p2.y-p1.y)*-1, p2.x-p1.x};
            return v;
        }
}
